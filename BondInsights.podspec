

Pod::Spec.new do |spec|

  
  spec.name         = "BondInsights"
  spec.version      = "1.0.0"
  spec.summary      = "BondInsights is iOS SDK from Bond.AI"
  spec.description  = "BondInsights is iOS SDK from Bond.AI, can be integrated in iOS application."
  spec.homepage     = "http://bond.ai"
  spec.license      = "MIT"
  spec.license      = { :type => "MIT", :file => "LICENSE.txt" }
  spec.author             = { "Mahesh Balshetwar" => "mahesh@bond.ai" }
  spec.platform     = :ios, "11.0"
  spec.swift_versions = "5.4"
  spec.source       = { :git => "https://bhushandeshmukh@bitbucket.org/bond-ai/cpbondinsights.git", :tag => "1.0.0" }
  #spec.source_files  = 'BondDashboard.framework/**/*'
  spec.resources = "BondDashboard.xcframework/**/*.{png,ttf,json}"
  
  #spec.public_header_files = 'BondDashboard.framework/Headers/*.h'
  spec.ios.vendored_frameworks = 'BondDashboard.xcframework'
  #spec.ios.deployment_target = '11.0'
  #spec.exclude_files = "BondDashboard.framework/**/*.{plist}"
  spec.requires_arc = true
  
  spec.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
  spec.dependency "BondBotSDKNew", "~> 1.3.3"

  
end
